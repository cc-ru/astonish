local function resolutionToTier(w, h)
  local w, h = math.max(w, h), math.min(w, h)

  if w <= 50 and h <= 16 then
    return 1
  elseif w <= 80 and h <= 25 then
    return 2
  else
    return 3
  end
end

local function depthToTier(depth)
  if depth == 1 then
    return 1
  elseif depth <= 4 then
    return 2
  else
    return 3
  end
end

local function extractChannels(color)
  local r = (color >> 16) & 0xff
  local g = (color >> 8) & 0xff
  local b = color & 0xff

  return r, g, b
end

local function inverseMap(tbl)
  local inverseMapping = {}

  for k, v in pairs(tbl) do
    assert(inverseMapping[v] == nil, ("value %s is not unique"):format(v))
    inverseMapping[v] = k
  end

  return inverseMapping
end

return {
  depthToTier = depthToTier,
  resolutionToTier = resolutionToTier,
  extractChannels = extractChannels,
  inverseMap = inverseMap,
}
