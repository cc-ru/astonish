local util = require("astonish.util")
local extractChannels = util.extractChannels
local inverseMap = util.inverseMap

local reds = 6
local greens = 8
local blues = 5

-- Get the perceived color difference.
local function colorDelta(color1, color2)
  local r1, g1, b1 = extractChannels(color1)
  local r2, g2, b2 = extractChannels(color2)

  local dr = r1 - r2
  local dg = g1 - g2
  local db = b1 - b2

  -- using the gamma value of 2, find the luminance difference in linear space
  return
    0.2126 * dr^2 +
    0.7152 * dg^2 +
    0.0722 * db^2
end

local function inflate(palette, index)
  return palette[index + 1]
end

local function map(palette, color)
  return palette:inflate(palette:deflate(color))
end

local deflate = {}

deflate[1] = function(self, color)
  local index = self.colors[color]

  if index then
    return index - 1
  end

  local minDelta = math.huge

  for i = 1, self.len, 1 do
    local delta = colorDelta(self[i], color)

    if delta < minDelta then
      index, minDelta = i, delta
    end
  end

  return index - 1
end

deflate[2] = deflate[1]

deflate[3] = function(self, color)
  local index = self.colors[color]

  if index then
    return index - 1
  end

  index = deflate[1](self, color)

  local r, g, b = extractChannels(color)
  local ir = (r * (reds - 1) / 0xff + 0.5) // 1
  local ig = (g * (greens - 1) / 0xff + 0.5) // 1
  local ib = (b * (blues - 1) / 0xff + 0.5) // 1

  local deflatedIndex = 16 +
    ir * greens * blues +
    ig * blues +
    ib

  if colorDelta(color, self:inflate(deflatedIndex)) <
      colorDelta(color, self:inflate(index)) then
    return deflatedIndex
  else
    return index
  end
end

local paletteConstructors = {}

paletteConstructors[1] = function(secondColor)
  local self = {
    0x000000,
    secondColor or 0xffffff,
  }

  return self
end

paletteConstructors[2] = function()
  local self = {
    0xffffff, 0xffcc33, 0xcc66cc, 0x6699ff,
    0xffff33, 0x33cc33, 0xff6699, 0x333333,
    0xcccccc, 0x336699, 0x9933cc, 0x333399,
    0x663300, 0x336600, 0xff3333, 0x000000,
  }

  return self
end


paletteConstructors[3] = function()
  local self = {}

  -- grayscale
  for i = 1, 16, 1 do
    self[i] = 0xff * i / (16 + 1) * 0x10101
  end

  for index = 16, 255, 1 do
    local i = index - 16

    local ib = i % blues
    local ig = (i // blues) % greens
    local ir = (i // blues // greens) % reds

    local r = ir * 0xff / (reds - 1)
    local g = ig * 0xff / (greens - 1)
    local b = ib * 0xff / (blues - 1)

    r = (r + 0.5) // 1
    g = (g + 0.5) // 1
    b = (b + 0.5) // 1

    self[index + 1] = (r << 16) | (g << 8) | b
  end

  return self
end

local function Palette(tier, ...)
  local self = paletteConstructors[tier](...)
  self.colors = inverseMap(self)
  self.len = #self

  self.deflate = deflate[tier]
  self.inflate = inflate
  self.map = map

  return self
end

return {
  t1 = Palette(1, 0xffffff),
  t2 = Palette(2),
  t3 = Palette(3),

  colorDelta = colorDelta,

  Palette = Palette,
}
