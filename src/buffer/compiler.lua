local function packStrip(x, y, fg, bg, homogeneous)
  homogeneous = homogeneous and 1 or 0

  return (homogeneous << 32) | (y << 24) | (x << 16) | (bg << 8) | fg
end

local function unpackStrip(value)
  return
    (value >> 16) & 0xff,
    (value >> 24) & 0xff,
    value & 0xff,
    (value >> 8) & 0xff,
    value >> 32 == 1
end

local function commitStrip(stripLine, x, y, fg, bg, str, stripLen, homogeneous)
  local len = #stripLine
  stripLine[len + 1] = packStrip(x, y, fg, bg, homogeneous)
  stripLine[len + 2] = table.concat(str, "", 1, stripLen)

  return -1, -1, 0
end

local function divideIntoStrips(buffer, strips)
  local colors = buffer.palette.colors
  local stripX = -1
  local stripY = -1
  local stripFg = -1
  local stripBg = -1
  local stripString = {}
  local stripLen = 0
  local homogeneous = true

  for y = buffer.y0, buffer.y1, 1 do
    local stripLine = {}

    for x = buffer.x0, buffer.x1, 1 do
      local diffIndex, diffSubindex

      if buffer.flipped then
        diffIndex, diffSubindex = buffer:getIndex(y, x, true)
      else
        diffIndex, diffSubindex = buffer:getIndex(x, y, true)
      end

      if buffer[diffIndex][diffSubindex] or
          buffer[diffIndex][diffSubindex + 1] then
        local char, fg, bg = buffer:__mergeDiff(x, y)

        if stripLen > 0 and (fg ~= stripFg or bg ~= stripBg) then
          stripFg, stripBg, stripLen = commitStrip(
            stripLine,
            stripX,
            stripY,
            colors[stripFg] - 1,
            colors[stripBg] - 1,
            stripString,
            stripLen,
            homogeneous
          )
        end

        if stripLen == 0 then
          stripX = x
          stripY = y
          stripFg = fg
          stripBg = bg
        else
          homogeneous = homogeneous and stripString[stripLen] == char
        end

        stripLen = stripLen + 1
        stripString[stripLen] = char
      elseif stripLen > 0 then
        stripFg, stripBg, stripLen = commitStrip(
          stripLine,
          stripX,
          stripY,
          colors[stripFg] - 1,
          colors[stripBg] - 1,
          stripString,
          stripLen,
          homogeneous
        )
      end
    end

    if stripLen > 0 then
      stripFg, stripBg, stripLen = commitStrip(
        stripLine,
        stripX,
        stripY,
        colors[stripFg] - 1,
        colors[stripBg] - 1,
        stripString,
        stripLen,
        homogeneous
      )
    end

    if #stripLine > 0 then
      strips[y] = stripLine
      strips.h = y
    end
  end
end

local function updateRectangle(rectangles, x0, y0, y1, str, fg, bg)
  local topLeft = ((y0 << 8) | x0) << 1
  local bottomLeft = ((y1 << 8) | x0) << 1

  if rectangles[topLeft] then
    y0 = rectangles[topLeft] >> 16
  end

  rectangles[topLeft] = nil
  rectangles[topLeft + 1] = nil
  rectangles[bottomLeft] = (y0 << 16) | (bg << 8) | fg
  rectangles[bottomLeft + 1] = str
end

local function mergeAdjacentStrips(strips)
  local rectangles = {}

  for y = 1, strips.h, 1 do
    local line1 = strips[y]

    if line1 then
      local line2 = strips[y + 1]

      if line2 then
        -- try merging
        local i, j = 1, 1

        repeat
          local strip1 = line1[i]
          local str1 = line1[i + 1]

          local strip2 = line2[j]
          local str2 = line2[j + 1]

          local x1, y1, fg1, bg1, homogeneous = unpackStrip(strip1)
          local x2, y2, fg2, bg2

          if strip2 then
            x2, y2, fg2, bg2 = unpackStrip(strip2)
          end

          if homogeneous and x1 == x2 and fg1 == fg2 and bg1 == bg2 and
              str1 == str2 then
            updateRectangle(rectangles, x1, y1, y2, str1, fg1, bg1)
            i = i + 2
            j = j + 2
          else
            if not x2 or x1 <= x2 then
              updateRectangle(rectangles, x1, y1, y1, str1, fg1, bg1)
              i = i + 2
            end

            if x2 and x1 >= x2 then
              j = j + 2
            end
          end
        until i > #line1
      else
        -- convert all strips to rectangles
        for i = 1, #line1, 2 do
          local x, y, fg, bg = unpackStrip(line1[i])
          local str = line1[i + 1]

          updateRectangle(rectangles, x, y, y, str, fg, bg)
        end
      end
    end
  end

  return rectangles
end

local function writeSet(textData, instructions, x, y, fg, bg, str)
  local textIndex = textData.n
  textData.n = textData.n + 1
  textData[textData.n] = str

  instructions.n = instructions.n + 1

  instructions[instructions.n] = (
    (textIndex << 42) |
    (y << 26) |
    (x << 18) |
    (bg << 10) |
    (fg << 2) |
    0x0
  )
end

local function writeFill(textData, instructions, x, y0, y1, fg, bg, str)
  local textIndex = textData.n
  textData.n = textData.n + 1
  textData[textData.n] = str

  instructions.n = instructions.n + 1

  instructions[instructions.n] = (
    (textIndex << 42) |
    (y0 << 34) |
    (y1 << 26) |
    (x << 18) |
    (bg << 10) |
    (fg << 2) |
    0x1
  )
end

local function compileInstructions(rectangles)
  local instructions = {n = 0}
  local textData = {n = 0}

  for bottomLeft, data in pairs(rectangles) do
    if bottomLeft & 0x1 == 0 then
      local x0 = (bottomLeft >> 1) & 0xff
      local y0 = data >> 16
      local y1 = bottomLeft >> 9
      local bg = (data >> 8) & 0xff
      local fg = data & 0xff
      local str = rectangles[bottomLeft + 1]
      local h = y1 - y0 + 1

      if h <= 2 then
        for y = y0, y1, 1 do
          writeSet(textData, instructions, x0, y, fg, bg, str)
        end
      else
        writeFill(textData, instructions, x0, y0, y1, fg, bg, str)
      end
    end
  end

  return instructions, textData
end

local function sortByColor(lhs, rhs)
  return (lhs >> 2) & 0xffff < (rhs >> 2) & 0xffff
end

local function reorderInstructions(instructions)
  table.sort(instructions, sortByColor)
end

local function mergeInstructionsAndTextData(instructions, textData)
  for i = #instructions, 1, -1 do
    local j = i * 2 - 1
    local instruction = instructions[i]
    local textIndex = instruction >> 42
    instructions[j] = instruction & ((1 << 42) - 1)
    instructions[j + 1] = textData[textIndex + 1]
  end

  return instructions
end

local function debugPrintRectangles(rectangles)
  print("x0", "y0", "x1", "y1", "fg", "bg")

  for k, v in pairs(rectangles) do
    if k & 1 == 0 then
      print((k >> 1) & 0xff, v >> 16, #rectangles[k + 1], k >> 9, v & 0xff, (v >> 8) & 0xf)
    end
  end
end

local function stripCompiler(buffer)
  local strips = {h = 0}
  divideIntoStrips(buffer, strips)

  local rectangles = mergeAdjacentStrips(strips)

  local instructions, textData = compileInstructions(rectangles)
  reorderInstructions(instructions)

  return mergeInstructionsAndTextData(instructions, textData)
end

return {
  stripCompiler = stripCompiler,
}
