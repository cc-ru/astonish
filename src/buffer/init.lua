local compilers = require("astonish.buffer.compiler")
local tieredStorages = require("astonish.buffer.storage")

local Palette = require("astonish.palette").Palette

local util = require("astonish.util")
local depthToTier = util.depthToTier
local resolutionToTier = util.resolutionToTier
local extractChannels = util.extractChannels

local utf8char = utf8.char
local utf8charpattern = utf8.charpattern
local utf8codes = utf8.codes

local max = math.max
local min = math.min

local Buffer do
  local methods = {
    -- Unpack the color stored in the buffer to the palatable fg, bg values.
    __unpackColor = function(self, packedColor)
      local fg = packedColor & 0xffffff
      local bg = (packedColor >> 24) & 0xffffff

      return fg, bg
    end,

    -- Pack the color into the internal representation.
    --
    -- All colors are encoded into a single 48-bit integer as follows:
    --
    -- - bits 1 to 24 encode the foreground's RGB value
    -- - bits 25 to 48 encode the background's RGB value
    __packColor = function(self, fg, bg)
      return (bg << 24) | fg
    end,

    -- Merge the changes from the diff buffer at the given position.
    __mergeDiff = function(self, x, y)
      local mainIndex, mainSubindex = self:getIndex(x, y)
      local diffIndex, diffSubindex = self:getIndex(x, y, true)

      local color = self[diffIndex][diffSubindex]
      local char = self[diffIndex][diffSubindex + 1]

      local fg, bg

      if color then
        fg, bg = self:__unpackColor(color)
        fg = self.palette:map(fg)
        bg = self.palette:map(bg)
        self[mainIndex][mainSubindex] = self:__packColor(fg, bg)
        self[diffIndex][diffSubindex] = nil
      else
        fg, bg = self:__unpackColor(self[mainIndex][mainSubindex])
      end

      if char then
        self[mainIndex][mainSubindex + 1] = char
        self[diffIndex][diffSubindex + 1] = nil
      else
        char = self[mainIndex][mainSubindex + 1]
      end

      return char, fg, bg
    end,

    -- The unchecked version of `set` that sets *one* character on the given
    -- coordinates.
    --
    -- `fg`, `bg`, and `line` may be `nil`, in which case they won't be changed.
    _set = function(self, x, y, fg, bg, char, alpha)
      if alpha == 0 then
        -- since the new cell is effectively invisible, short-circuit
        return
      end

      alpha = alpha or 1

      if self.flipped then
        x, y = y, x
      end

      local mainIndex, mainSubindex = self:getIndex(x, y)
      local diffIndex, diffSubindex = self:getIndex(x, y, true)

      local mainChar = self[mainIndex][mainSubindex + 1]
      local mainColor = self[mainIndex][mainSubindex]
      local diffColor = self[diffIndex][diffSubindex]

      -- unlike the main buffer, elements of the diff buffer can be, and usually
      -- are, empty
      local oldColor = diffColor or mainColor

      if (not oldColor or not mainChar) and
          (not fg or not bg or not char or alpha ~= 1) then
        error("attempt to re-use data on an uninitialized buffer")
      end

      local oldBg, oldFg = oldColor and self:__unpackColor(oldColor)

      fg = fg or oldFg
      bg = bg or oldBg

      if alpha < 1 then
        if char then
          -- if we're provided with the char, blend oldBg and fg
          -- the oldChar is effectively discarded
          fg = self:alphaBlend(oldBg, fg, alpha)
        else
          -- if we're reusing the char, however, blend the foregrounds
          fg = self:alphaBlend(oldFg, fg, alpha)
        end

        bg = self:alphaBlend(oldBg, bg, alpha)
      end

      local packedColor = self:__packColor(fg, bg)

      if packedColor == mainColor then
        packedColor = nil
      end

      self[diffIndex][diffSubindex] = packedColor

      if char then
        if char == mainChar then
          char = nil
        end

        self[diffIndex][diffSubindex + 1] = char
      end
    end,

    -- The unchecked version of `get` that returns the char and the packed
    -- color.
    --
    -- The colors are palette-mapped unless `unmapped` is truthy.
    _get = function(self, x, y, unmapped)
      if self.flipped then
        x, y = y, x
      end

      local mainIndex, mainSubindex = self:getIndex(x, y)
      local diffIndex, diffSubindex = self:getIndex(x, y, true)

      local mainColor = self[mainIndex][mainSubindex]
      local mainChar = self[mainIndex][mainSubindex + 1]
      local diffColor = self[diffIndex][diffSubindex]
      local diffChar = self[diffIndex][diffSubindex + 1]

      local char = diffChar or mainChar
      local color = mainColor

      if not (mainColor or diffColor) or not char then
        error(
          ("attempt to index an uninitialized cell at (%d, %d)"):format(x, y)
        )
      end

      if diffColor then
        local fg, bg = self:__unpackColor(diffColor)
        fg = self.palette:map(fg)
        bg = self.palette:map(bg)
        color = self:__packColor(fg, bg)
      end

      return char, color
    end,

    -- The unchecked version of `fill`.
    --
    -- `x0`, `y0` are the top-left corner coordinates, and `x1`, `y1` the
    -- bottom-right. The other parameters have the same meaning as in `set`.
    _fill = function(self, x0, y0, x1, y1, fg, bg, char, alpha)
      for x = x0, x1, 1 do
        for y = y0, y1, 1 do
          self:_set(x, y, fg, bg, char, alpha)
        end
      end
    end,

    alphaBlend = function(color1, color2, alpha)
      if color1 == color2 then
        return color1
      end

      if alpha == 0 then
        return color1
      end

      if alpha == 1 then
        return color2
      end

      local ialpha = 1 - alpha
      local r1, g1, b1 = extractChannels(color1)
      local r2, g2, b2 = extractChannels(color2)

      -- convert the values into the linear space
      local igamma = 2.2
      local gamma = 1/2.2
      r1, g1, b1 = r1^igamma, g1^igamma, b1^igamma
      r2, g2, b2 = r2^igamma, g2^igamma, b2^igamma

      local r = (r1 * ialpha + r2 * alpha)^gamma
      local g = (g1 * ialpha + g2 * alpha)^gamma
      local b = (b1 * ialpha + b2 * alpha)^gamma

      -- round to the nearest integer
      r = (r + 0.5) // 1
      g = (g + 0.5) // 1
      b = (b + 0.5) // 1

      return (r << 16) | (g << 8) | b
    end,

    inBounds = function(self, x, y)
      return x >= self.x0 and x <= self.x1 and
        y >= self.y0 and y <= self.y1
    end,

    -- Set a line of characters.
    --
    -- `fg`, `bg`, and `line` may be `nil`, in which case they won't be changed.
    set = function(self, x0, y0, fg, bg, line, alpha, vertical)
      if line then
        local x, y = x0, y0

        for _, code in utf8codes(line) do
          if not self:inBounds(x, y) then
            return
          end

          self:_set(x, y, fg, bg, utf8char(code), alpha)

          if vertical then
            y = y + 1
          else
            x = x + 1
          end
        end
      else
        if not self:inBounds(x, y) then
          return
        end

        return self:_set(x0, y0, fg, bg, nil, alpha)
      end
    end,

    -- Get the character and its foreground and background colors at the given
    -- position.
    --
    -- The colors are mapped to the palette colors, unless `unmapped` is truthy.
    get = function(self, x, y, unmapped)
      if not self:inBounds(x, y) then
        return nil
      end

      local char, color = self:_get(x, y, unmapped)

      local fg, bg = self:__unpackColor(color)

      return char, fg, bg
    end,

    -- Fill a rectangular area.
    --
    -- `x0`, `y0` and `x1`, `y1` specify the two opposite corners of the
    -- rectangle. The other parameters have the same meaning as in `set`.
    fill = function(self, x0, y0, x1, y1, fg, bg, char, alpha)
      if x0 > x1 then
        x0, x1 = x1, x0
      end

      if y0 > y1 then
        y0, y1 = y1, y0
      end

      x0 = max(self.x0, x0)
      x1 = min(self.x1, x1)

      y0 = max(self.y0, y0)
      y1 = min(self.y1, y1)

      if char then
        char = char:match(utf8charpattern)
      end

      return self:_fill(x0, y0, x1, y1, fg, bg, char, alpha)
    end,

    -- Reset the diff buffer, undoing all changes.
    resetDiffs = function(self)
      for x = self.x0, self.x1, 1 do
        for y = self.y0, self.y1, 1 do
          if self.flipped then
            x, y = y, x
          end

          local diffIndex, diffSubindex = self:getIndex(x, y, true)
          self[diffIndex][diffSubindex] = nil
          self[diffIndex][diffSubindex + 1] = nil
        end
      end
    end,

    -- Copy an area from another buffer onto self.
    --
    -- If the `source` is self, handles overlapping regions.
    --
    -- The area defaults to the buffer's resolution if not specified.
    copyFrom = function(self, source, sx0, sy0, sx1, sy1, x0, y0)
      if not sx0 then
        x0, y0 = 1, 1
      end

      if not sx1 then
        sx0, sy0 = 1, 1
      end

      if not x0 then
        sx1 = source.w
        sy1 = source.h
      end

      if sx0 > sx1 then
        sx0, sx1 = sx1, sx0
      end

      if sy0 > sy1 then
        sy0, sy1 = sy1, sy0
      end

      sx0 = max(source.x0, sx0)
      sy0 = max(source.y0, sy0)

      x0 = max(self.x0, x0)
      y0 = max(self.y0, y0)

      sx1 = min(source.x1, sx1)
      sy1 = min(source.y1, sy1)

      local w = min(sx1 - sx0 + 1, self.w)
      local h = min(sy1 - sy0 + 1, self.h)

      sx1 = min(sx0 + w - 1, sx1)
      sy1 = min(sy0 + h - 1, sy1)

      local xStart, xEnd, xStep = 1, w, 1
      local yStart, yEnd, yStep = 1, h, 1

      if src == self then
        if x0 > sx0 then
          xStart, xEnd, xStep = w, 1, -1
        end

        if y0 > sy0 then
          yStart, yEnd, yStep = h, 1, -1
        end
      end

      for x = xStart, xEnd, xStep do
        for y = yStart, yEnd, yStep do
          local localX = x0 + x - 1
          local localY = y0 + y - 1
          local remoteX = sx0 + x - 1
          local remoteY = sy0 + y - 1

          local char, fg, bg = src:get(remoteX, remoteY)

          self:_set(localX, localY, fg, bg, char)
        end
      end
    end,

    -- Create a new buffer and copy an area onto it.
    clone = function(self, x0, y0, x1, y1)
      if not x0 then
        x0, y0, x1, y1 = 1, 1, self.w, self.h
      elseif not x1 then
        x0, y0, x1, y1 = 1, 1, x0, y0
      end

      if x0 > x1 then
        x0, x1 = x1, x0
      end

      if y0 > y1 then
        y0, y1 = y1, y0
      end

      x0 = max(self.x0, x0)
      y0 = max(self.y0, x0)

      x1 = min(self.x1, x1)
      y1 = min(self.y1, y1)

      local w = x1 - x0 + 1
      local h = y1 - y0 + 1

      local new = Buffer(w, h, self.depth)
      new.palette = self.palette

      new:copyFrom(self, x0, y0, x1, y1)
    end,

    flush = function(self, gpu, sx, sy)
      sx = sx or 1
      sy = sy or 1

      local palette = self.palette
      local log = self.log

      local instructions = self:__instructionCompiler()
      local instructionLen = #instructions

      local i = 1

      local currentFg = gpu.getForeground()
      local currentBg = gpu.getBackground()

      while i <= instructionLen do
        local instruction = instructions[i]
        local opcode = instruction & 0x3

        if opcode == 0x0 then
          -- set: [y: 8] [x: 8] [bg index: 8] [fg index: 8] [opcode: 2]
          -- all values are 0-based
          local y = (instruction >> 26) & 0xff
          local x = (instruction >> 18) & 0xff
          local bg = palette[((instruction >> 10) & 0xff) + 1]
          local fg = palette[((instruction >> 2) & 0xff) + 1]

          i = i + 1
          local text = instructions[i]

          if fg ~= currentFg then
            gpu.setForeground(fg)
            currentFg = fg
          end

          if bg ~= currentBg then
            gpu.setBackground(bg)
            currentBg = bg
          end

          assert(gpu.set(sx + x, sy + y, text))
        elseif opcode == 0x1 then
          -- fill: [y0: 8] [y1: 8] [x: 8] [bg index: 8] [fg index: 8] [opcode: 2]
          -- all values are 0-based
          local y0 = (instruction >> 34) & 0xff
          local y1 = (instruction >> 26) & 0xff
          local x = (instruction >> 18) & 0xff
          local bg = palette[((instruction >> 10) & 0xff) + 1]
          local fg = palette[((instruction >> 2) & 0xff) + 1]

          i = i + 1
          local text = instructions[i]

          if fg ~= currentFg then
            gpu.setForeground(fg)
            currentFg = fg
          end

          if bg ~= currentBg then
            gpu.setBackground(bg)
            currentBg = bg
          end

          assert(gpu.fill(
            sx + x - 1,
            sy + y0 - 1,
            #text,
            y1 - y0 + 1,
            (text:match(utf8charpattern))
          ))
        else
          error(("unknown opcode: %x"):format(opcode))
        end

        i = i + 1
      end
    end,
  }

  function Buffer(w, h, depth)
    local tier = resolutionToTier(w, h)
    local self = tieredStorages[tier].new()

    -- usually, methods are attached using the __index metamethod;
    -- we paste them into the object here to avoid metatable lookup overhead,
    -- which does matter if you're doing it a million times per second
    self.getIndex = tieredStorages[tier].index

    for k, v in pairs(methods) do
      self[k] = v
    end

    -- make sure view.lua is kept in sync with these
    self.x0 = 1
    self.y0 = 1
    self.x1 = w
    self.y1 = h
    self.w = w
    self.h = h
    self.depth = depth
    self.palette = Palette(depthToTier(depth))

    self.__instructionCompiler = compilers.stripCompiler

    -- if the width is less than the height, we flip the buffer
    self.flipped = w < h

    return self
  end
end

return {
  Buffer = Buffer,
}
