local BufferView do
  local methods = {
    _set = function(self, rx, ry, fg, bg, char, alpha)
      local x, y = self:toAbsoluteCoords(rx, ry)

      return self.buffer:_set(x, y, fg, bg, char, alpha)
    end,

    _get = function(self, rx, ry, unmapped)
      local x, y = self:toAbsoluteCoords(rx, ry)

      return self.buffer:_get(x, y, unmapped)
    end,

    toAbsoluteCoords = function(self, rx, ry)
      return self.coordX + rx - 1, self.coordY + ry - 1
    end,

    inBounds = function(self, rx, ry)
      local x, y = self:toAbsoluteCoords(x, y)

      return self.x0 <= x and x <= self.x1 and
        self.y0 <= y and y <= self.y1
    end,

    getIndex = function(self, rx, ry, diff)
      local x, y

      if self.flipped then
        x, y = self:toAbsoluteCoords(ry, rx)
      else
        x, y = self:toAbsoluteCoords(rx, ry)
      end

      return self.buffer:getIndex(x, y, diff)
    end,
  }

  function BufferView(buffer, x0, y0, w, h, coordX, coordY)
    assert(w >= 0 and h >= 0, "width and height must be non-negative")

    local self = {
      buffer = buffer,
      x0 = x0,
      y0 = y0,
      x1 = x0 + w - 1,
      y1 = y0 + h - 1,
      coordX = coordX,
      coordY = coordY,
    }

    -- usually, methods are attached using the __index metamethod;
    -- we paste them into the object here to avoid metatable lookup overhead,
    -- which does matter if you're doing it a million times per second
    for k, v in pairs(methods) do
      self[k] = v
    end

    assert(
      buffer:inBounds(self:toAbsoluteCoords(x0, y0)) and
      buffer:inBounds(self:toAbsoluteCoords(self.x1, self.y1)),
      "attempt to create a view outside the buffer"
    )

    self.w = w
    self.h = h
    self.depth = depth
    self.palette = buffer.palette
    self.flipped = buffer.flipped

    self.__unpackColor = buffer.__unpackColor
    self.__packColor = buffer.__packColor
    self.__mergeDiff = buffer.__mergeDiff
    self.set = buffer.set
    self.get = buffer.get
    self.fill = buffer.fill
    self.resetDiffs = buffer.resetDiffs
    self.copyFrom = buffer.copyFrom
    self.clone = buffer.clone

    return self
  end
end

return {
  BufferView = BufferView,
}
