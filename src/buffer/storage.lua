local tieredStorages = {}

tieredStorages[1] = {
  new = function()
    -- 2 × 50 × 16 elements, twice for the 2 buffers, is 3200,
    -- split in 3 tables (1024, 1024, and 1152 elements in each)
    local element = "nil,"

    local code = ("return {{%s}, {%s}, {%s}}"):format(
      element:rep(1024),
      element:rep(1024),
      element:rep(2 * 2 * 50 * 16 - 2 * 1024)
    )

    return load(code, nil, "t")()
  end,

  index = function(self, x, y, diff)
    local index = 2 * ((y - 1) * 50 + x - 1)

    if diff then
      index = index + 1600
    end

    if i <= 2048 then
      return i // 1024 + 1,  -- the index of the subtable
        i % 1024 + 1  -- the index of the element in the subtable
    end

    -- the last one is a bit bigger than the others; requires special treatment
    return 3,  -- the last subtable
        i - 2048 + 1
  end,
}

tieredStorages[2] = {
  new = function()
    -- 2 × 80 × 25 elements, twice for the 2 buffers, is 8000,
    -- split in 2 tables (4000 elements in each)
    local element = "nil,"

    local code = ("return {{%s}, {%s}}"):format(
      element:rep(4000),
      element:rep(4000)
    )

    return load(code, nil, "t")()
  end,

  index = function(self, x, y, diff)
    local index = 2 * ((y - 1) * 80 + x - 1)

    if diff then
      index = index + 4000
    end

    return index // 4000 + 1,
      index % 4000 + 1
  end,
}

tieredStorages[3] = {
  new = function()
    -- 2 × 160 × 50 elements, twice for the 2 buffers, is 32000,
    -- split in 32 tables with a thousand elements each
    local element = "nil,"
    local subtable = ("{%s},"):format(element:rep(1000))
    local code = ("return {%s}"):format(subtable:rep(32))

    return load(code, nil, "t")()
  end,

  index = function(self, x, y, diff)
    local index = 2 * ((y - 1) * 160 + x - 1)

    if diff then
      index = index + 16000
    end

    return index // 1000 + 1,
      index % 1000 + 1
  end,
}

return tieredStorages
