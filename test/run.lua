local fs = require("filesystem")

local selfPath = os.getenv("_")
local testPath = fs.path(selfPath)

local test = dofile(fs.concat(testPath, "init.lua"))

local environment

local requireImpl do
  local loaded, loading = {
    component = require("component"),
    computer = require("computer"),
    bit32 = require("bit32"),
    package = require("package")
  }, {}

  function requireImpl(module)
    checkArg(1, module, "string")
    if loaded[module] ~= nil then
      return loaded[module]
    elseif not loading[module] then
      local library, status, step

      step, library, status = "not found", package.searchpath(module, package.path)

      if library then
        step, library, status = "loadfile failed", loadfile(
          library, "t", setmetatable({
            require = requireImpl,
            dbg = environment.dbg,
          }, {__index = _G})
        )
      end

      if library then
        loading[module] = true
        step, library, status = "load failed", pcall(library, module)
        loading[module] = false
      end

      assert(library, string.format("module '%s' %s:\n%s", module, step, status))
      loaded[module] = status
      return status
    else
      error("already loading: " .. module .. "\n" .. debug.traceback(), 2)
    end
  end
end

environment = setmetatable({
  assert = test.assert,
  test = test.test,
  time = test.time,
  timeThis = test.timeThis,
  require = requireImpl,
}, {__index = _G})

environment.dbg = requireImpl("debugger")

local function runFilesInDirectory(path, blacklist)
  for node in fs.list(path) do
    local nodePath = fs.concat(path, node)

    if fs.isDirectory(nodePath) then
      runFilesInDirectory(nodePath)
    else
      local blacklisted = false

      for _, v in pairs(blacklist or {}) do
        if fs.canonical(nodePath) == fs.canonical(v) then
          blacklisted = true
          break
        end
      end

      if not blacklisted then
        local ok
        local chunk, err = loadfile(nodePath, "t", environment)

        if chunk then
          ok, err = xpcall(chunk, debug.traceback)
        end

        if not ok then
          io.stderr:write(("Failed to load %s: %s\n"):format(
            nodePath,
            err
          ))
        end
      end
    end
  end
end

runFilesInDirectory(testPath, {
  fs.concat(testPath, "init.lua"),
  fs.concat(testPath, "run.lua"),
})

test.run(...)
