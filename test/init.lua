local computer = require("computer")
local event = require("event")

local registeredTests = {}

local function getSource()
  for level = 0, math.huge, 1 do
    local frameInfo = debug.getinfo(level, "S")

    if not frameInfo then
      return nil
    end

    if frameInfo.what == "main" and frameInfo.source then
      if frameInfo.source:sub(1, 1) == "=" then
        return frameInfo.short_src
      end
    end
  end
end

local function test(name, options, code)
  assert(registeredTests[name] == nil, ("duplicate key: %s"):format(name))

  if not code then
    options, code = {}, options
  end

  local source = getSource()

  local obj = {
    name = name,
    run = code,
    source = source,
    options = options,
  }

  registeredTests[name] = obj

  return obj
end

local function time(f)
  local start = computer.uptime()
  local startClock = os.clock()

  f()

  return {
    uptime = computer.uptime() - start,
    clock = os.clock() - startClock,
  }
end

local function run(filter)
  local testsToRun = {}
  local errors = {}
  local status = {
    skipped = 0,
    passed = 0,
    failed = 0,
    partial = 0,
    errored = 0,
  }

  for k, v in pairs(registeredTests) do
    if not filter or k:match(filter) then
      table.insert(testsToRun, v)
    else
      status.skipped = status.skipped + 1
    end
  end

  table.sort(testsToRun, function(lhs, rhs)
    if lhs.source == rhs.source then
      return lhs.name < rhs.name
    else
      return not lhs.source or lhs.source < rhs.source
    end
  end)

  local lastSource

  for i, test in ipairs(testsToRun) do
    if test.source ~= lastSource then
      if lastSource then
        io.stderr:write("\n")
      end

      io.stderr:write(("\027[0mRunning tests in %s:\n"):format(test.source))

      lastSource = test.source
    end

    io.stderr:write(
      ("\027[0m  [%d/%d] Test %s: \027[33m..."):format(
        i,
        #testsToRun,
        test.name
      )
    )

    local ok, err = xpcall(test.run, function(err)
      if type(err) == "table" and err.testCaused then
        err.reason = debug.traceback(err.reason, 2)

        return err
      else
        return {
          testCaused = false,
          reason = debug.traceback(err, 2),
        }
      end
    end)

    if not ok then
      errors[test.name] = err or "unknown error"
    end

    io.stderr:write("\027[3D\027[K") -- 3 chars back

    local state = "passed"

    if not ok then
      if err.testCaused then
        state = "failed"
      else
        state = "errored"
      end
    end

    local message = state

    if state == "passed" and test.options.time then
      local max = math.max(err.uptime, err.clock)

      if max <= test.options.time[1] then
        state = "passed"
      elseif max <= test.options.time[2] then
        state = "partial"
      else
        state = "failed"
      end

      message = ("%.2f s / %.2f s clock"):format(err.uptime, err.clock)
    end

    if test.options.shouldFail and state == "failed" or
        test.options.shouldError and state == "errored" or
        not (test.options.shouldFail or test.options.shouldError) and
        state == "passed" then
      state = "passed"
    end

    if state == "passed" then
      io.stderr:write("\027[32m" .. message)
    elseif state == "partial" then
      io.stderr:write("\027[33m" .. message)
    elseif state == "failed" or state == "errored" then
      io.stderr:write("\027[31m" .. message)
    end

    print()

    status[state] = status[state] + 1
  end

  io.stderr:write((
    "\027[0m" ..
    "\027[32m%d\027[0m passed, " ..
    "\027[33m%d\027[0m passed partially, " ..
    "\027[31m%d\027[0m failed, " ..
    "\027[31m%d\027[0m errored, " ..
    "%d skipped\n"
  ):format(
    status.passed, status.partial, status.failed, status.errored, status.skipped
  ))

  if status.failed + status.errored > 0 then
    io.stderr:write("\n")
  end

  for name, err in pairs(errors) do
    io.write("Press ^C to exit, or Enter to continue...")

    repeat
      local evt = {event.pullMultiple("key_down", "interrupted")}

      if evt[1] == "interrupted" then
        goto exit
      end
    until evt[4] == 28

    print(("\r\027[KTest %s \027[31m%s\027[0m: \027[31m%s\027[0m"):format(
      name,
      err.testCaused and "failed" or "errored",
      (err.reason:gsub("\n\t", "\n    "):gsub("\n", "\n    "))
    ))
  end

  ::exit::

  if status.failed + status.errored > 0 then
    os.exit(1)
  else
    os.exit(0)
  end
end

local function assert(condition, message)
  if not condition then
    error({
      testCaused = true,
      reason = message or "assertion failed",
    }, 1)
  end
end

return {
  test = test,
  time = time,
  run = run,
  assert = assert,
}
