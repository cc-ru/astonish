local com = require("component")

local Buffer = require("astonish.buffer").Buffer

local function withBuffer(w, h, depth, f)
  if not h then
    w, h, depth, f = 160, 50, 8, w
  end

  return function(...)
    local buf = Buffer(w, h, depth)

    local primaryGpu = com.getPrimary("gpu").address
    local primaryScreen = com.getPrimary("screen").address
    local gpu, screen

    for addr in com.list("gpu") do
      if addr ~= primaryGpu then
        gpu = com.proxy(addr)
        break
      end
    end

    for addr in com.list("screen") do
      if addr ~= primaryScreen then
        screen = addr
        break
      end
    end

    assert(gpu, "no secondary GPU found")
    assert(screen, "no secondary screen found")

    gpu.bind(screen)

    gpu.setBackground(0x000000)
    gpu.setForeground(0xffffff)
    gpu.fill(1, 1, 160, 50, " ")

    return f(buf, gpu, ...)
  end
end

local function assertArea(buf, gpu, x0, y0, x1, y1, predicate, bufferOnly)
  for y = y0, y1, 1 do
    for x = x0, x1, 1 do
      local char, fg, bg = buf:get(x, y)
      local ok, err = predicate(char, fg, bg)

      if not ok then
        assert(false, ("predicate failed at (%d, %d): %s"):format(
          x, y, err or ("the predicate returned %s"):format(ok)
        ))
      end

      if not bufferOnly then
        local gpuChar, gpuFg, gpuBg = gpu.get(x, y)

        if not (gpuChar == char and gpuFg == fg and gpuBg == bg) then
          assert(false, (
            "GPU differs from the buffer at (%d, %d): " ..
            "buffer { fg = %06x, bg = %06x, char = %q } " ..
            "gpu { fg = %06x, bg = %06x, char = %q }"
          ):format(x, y, fg, bg, char, gpuFg, gpuBg, gpuChar))
        end
      end
    end
  end
end

local function compare(...)
  local values = table.pack(...)

  return function(...)
    local args = table.pack(...)

    if args.n ~= values.n then
      return false, ("expected %d values, given %d"):format(values.n, args.n)
    end

    for i = 1, args.n, 1 do
      if args[i] ~= values[i] then
        return false,
          ("values #%d differ: %s and %s"):format(i, args[i], values[i])
      end
    end

    return true
  end
end

local function hsvToRgb(hue, saturation, value)
  local chroma = saturation * value
  local normalizedHue = (hue / 60) % 6
  local secondComponent = chroma * (1 - math.abs(normalizedHue % 2 - 1))

  local r, g, b

  if 0 <= normalizedHue and normalizedHue < 1 then
    r, g, b = chroma, secondComponent, 0
  elseif 1 <= normalizedHue and normalizedHue < 2 then
    r, g, b = secondComponent, chroma, 0
  elseif 2 <= normalizedHue and normalizedHue < 3 then
    r, g, b = 0, chroma, secondComponent
  elseif 3 <= normalizedHue and normalizedHue < 4 then
    r, g, b = 0, secondComponent, chroma
  elseif 4 <= normalizedHue and normalizedHue < 5 then
    r, g, b = secondComponent, 0, chroma
  elseif 5 <= normalizedHue and normalizedHue < 6 then
    r, g, b = chroma, 0, secondComponent
  end

  local difference = value - chroma

  return
    ((255 * (r + difference) // 1) << 16) |
    ((255 * (g + difference) // 1) << 8) |
    (255 * (b + difference) // 1)
end

test("creating buffer", withBuffer(function(buf, gpu) end))

test("filling the whole buffer", withBuffer(function(buf, gpu)
  buf:fill(1, 1, 160, 50, 0xffffff, 0x000000, " ")

  assertArea(buf, gpu, 1, 1, 160, 50, compare(" ", 0xffffff, 0x000000), true)
end))

test("drawing 4 rectangles", withBuffer(function(buf, gpu)
  buf:fill(1, 1, 160, 50, 0x000000, 0xffffff, " ")

  buf:fill(21, 7, 21 + 40 - 1, 7 + 13 - 1, 0x000000, 0xff0000, " ")
  buf:fill(101, 7, 101 + 40 - 1, 7 + 13 - 1, 0x000000, 0x00ff00, " ")
  buf:fill(21, 32, 21 + 40 - 1, 32 + 13 - 1, 0x000000, 0xffff00, " ")
  buf:fill(101, 32, 101 + 40 - 1, 32 + 13 - 1, 0x000000, 0x00ffff, " ")

  buf:flush(gpu)

  -- horizontal strips
  assertArea(buf, gpu, 1, 1, 160, 6, compare(" ", 0x000000, 0xffffff))
  assertArea(buf, gpu, 1, 20, 160, 31, compare(" ", 0x000000, 0xffffff))
  assertArea(buf, gpu, 1, 45, 160, 50, compare(" ", 0x000000, 0xffffff))

  -- vertical strips
  assertArea(buf, gpu, 1, 1, 20, 50, compare(" ", 0x000000, 0xffffff))
  assertArea(buf, gpu, 61, 1, 100, 50, compare(" ", 0x000000, 0xffffff))
  assertArea(buf, gpu, 141, 1, 160, 50, compare(" ", 0x000000, 0xffffff))

  -- rectangles
  assertArea(buf, gpu, 21, 7, 60, 19, compare(" ", 0x000000, 0xff0000))
  assertArea(buf, gpu, 101, 7, 140, 19, compare(" ", 0x000000, 0x00ff00))
  assertArea(buf, gpu, 21, 32, 60, 44, compare(" ", 0x000000, 0xffff00))
  assertArea(buf, gpu, 101, 32, 140, 44, compare(" ", 0x000000, 0x00ffff))
end))

test("re-using the buffer", withBuffer(function(buf, gpu)
  buf:fill(1, 1, 160, 50, 0xffffff, 0x000000, " ")

  for i = 0, 10, 1 do
    local color = hsvToRgb(i * 36, 1, 1)
    buf:fill(1, 1, 20, 10, 0xffffff, color, " ")
    buf:flush(gpu)

    assertArea(
      buf, gpu, 1, 1, 20, 10,
      compare(" ", 0xffffff, buf.palette:map(color))
    )

    assertArea(buf, gpu, 1, 11, 20, 50, compare(" ", 0xffffff, 0x000000))
    assertArea(buf, gpu, 21, 1, 160, 50, compare(" ", 0xffffff, 0x000000))

    os.sleep(0)
  end
end))

test("filling one rectangle 12 times", {
  time = {0.1, 0.2},
}, withBuffer(
  function(buf, gpu)
    buf:fill(1, 1, 160, 50, 0xffffff, 0x000000, " ")
    buf:flush(gpu)

    return time(function()
      for i = 1, 12, 1 do
        local color = hsvToRgb(math.random(0, 360), math.random(0.5, 1), 1)

        buf:fill(10, 5, 150, 45, 0xffffff, color, " ")
        buf:set(76, 3, color, 0x000000, ("%06x"):format(color))
        buf:flush(gpu)
      end
    end)
  end
))
